import sys, random
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QLineEdit, QLabel, QComboBox, QPushButton, QTabWidget, \
    QVBoxLayout, QTableWidget, \
    QTableWidgetItem, QHBoxLayout, QSizePolicy, QStatusBar, QFrame, QDialog, QPlainTextEdit, QDateEdit
from PyQt5.QtGui import *
from PyQt5.QtCore import pyqtSlot, QRect,QSize, QCoreApplication, Qt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from api import Contacts, Invoices


class InvoiceView():
    def __init__(self):
        self.contact = Contacts()
        self.api_invoice = Invoices()
        pass

    def add_quote(self,MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(821, 512)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.response_label = QLabel(self.centralwidget)
        self.response_label.setGeometry(QRect(280, 7, 431, 17))
        self.response_label.setObjectName("response_label")

        self.frame_2 = QFrame(self.centralwidget)
        self.frame_2.setGeometry(QRect(420, 30, 361, 371))
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.lineEdit_3 = QLineEdit(self.frame_2)
        self.lineEdit_3.setGeometry(QRect(120, 60, 201, 29))
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.label_7 = QLabel("Item",self.frame_2)
        self.label_7.setGeometry(QRect(80, 65, 31, 17))
        self.label_7.setObjectName("label_7")
        self.plainTextEdit = QPlainTextEdit(self.frame_2)
        self.plainTextEdit.setGeometry(QRect(120, 100, 201, 75))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.label_8 = QLabel("Description",self.frame_2)
        self.label_8.setGeometry(QRect(20, 130, 81, 20))
        self.label_8.setObjectName("label_8")
        self.lineEdit_4 = QLineEdit(self.frame_2)
        self.lineEdit_4.setGeometry(QRect(120, 190, 201, 29))
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.label_9 = QLabel("Quantity",self.frame_2)
        self.label_9.setGeometry(QRect(40, 200, 61, 17))
        self.label_9.setObjectName("label_9")
        self.lineEdit_5 = QLineEdit(self.frame_2)
        self.lineEdit_5.setGeometry(QRect(120, 230, 201, 29))
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.label_10 = QLabel("Unit Price",self.frame_2)
        self.label_10.setGeometry(QRect(40, 240, 71, 20))
        self.label_10.setObjectName("label_10")
        self.lineEdit_6 = QLineEdit(self.frame_2)
        self.lineEdit_6.setGeometry(QRect(120, 270, 201, 29))
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_11 = QLabel("Account",self.frame_2)
        self.label_11.setGeometry(QRect(40, 280, 61, 20))
        self.label_11.setObjectName("label_11")
        self.lineEdit_7 = QLineEdit(self.frame_2)
        self.lineEdit_7.setGeometry(QRect(120, 310, 201, 29))
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.label_12 = QLabel("Tax Rate",self.frame_2)
        self.label_12.setGeometry(QRect(50, 320, 61, 20))
        self.label_12.setObjectName("label_12")
        self.label_13 = QLabel("Line item Information",self.frame_2)
        self.label_13.setGeometry(QRect(130, 10, 151, 31))
        self.label_13.setObjectName("label_13")
        self.frame = QFrame(self.centralwidget)
        self.frame.setGeometry(QRect(40, 30, 341, 351))
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")
        self.comboBox = QComboBox(self.frame)
        self.comboBox.setGeometry(QRect(120, 70, 181, 31))
        self.comboBox.setObjectName("comboBox")
        for contact in self.contact.contacts_get():
            for key, value in contact:
                if key == 'Name':
                    self.comboBox.addItem(value)
        self.label = QLabel("Customer",self.frame)
        self.label.setGeometry(QRect(47, 75, 71, 17))
        self.label.setObjectName("label")
        self.dateEdit = QDateEdit(self.frame)
        self.dateEdit.setGeometry(QRect(120, 120, 181, 29))
        self.dateEdit.setObjectName("dateEdit")
        self.label_2 = QLabel("Date",self.frame)
        self.label_2.setGeometry(QRect(78, 125, 41, 17))
        self.label_2.setObjectName("label_2")
        self.dateEdit_2 = QDateEdit(self.frame)
        self.dateEdit_2.setGeometry(QRect(120, 170, 181, 29))
        self.dateEdit_2.setObjectName("dateEdit_2")
        self.label_3 = QLabel("Expiry",self.frame)
        self.label_3.setGeometry(QRect(68, 175, 45, 17))
        self.label_3.setObjectName("label_3")
        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setGeometry(QRect(120, 220, 181, 29))
        self.lineEdit.setObjectName("lineEdit")
        self.label_4 = QLabel("Quote Number",self.frame)
        self.label_4.setGeometry(QRect(10, 220, 101, 20))
        self.label_4.setObjectName("label_4")
        self.lineEdit_2 = QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QRect(120, 270, 181, 29))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_5 = QLabel("Reference",self.frame)
        self.label_5.setGeometry(QRect(40, 270, 71, 20))
        self.label_5.setObjectName("label_5")
        self.label_6 = QLabel("Quote Information",self.frame)
        self.label_6.setGeometry(QRect(90, 20, 131, 31))
        self.label_6.setObjectName("label_6")
        self.pushButton = QPushButton("Add Invoice",self.centralwidget)
        self.pushButton.setGeometry(QRect(320, 430, 131, 31))
        self.pushButton.setStyleSheet("color: rgb(0,0,0);\n"
                                        "background-color: #a5d5f1;")
        self.pushButton.setObjectName("pushButton_4")
        self.pushButton.clicked.connect(self.add_invoice)
        MainWindow.setCentralWidget(self.centralwidget)

    def add_invoice(self):
        invoice_info = {
            'customer' : self.comboBox.currentText().strip(),
            'date' : self.dateEdit.text(),
            'expiry' : self.dateEdit_2.text(),
            'quote_number' : self.lineEdit.text(),
            'reference' : self.lineEdit_2.text(),
            'item' : self.lineEdit_3.text(),
            'description' : self.plainTextEdit.toPlainText(),
            'quantity' : self.lineEdit_4.text(),
            'price': self.lineEdit_5.text(),
            'account': self.lineEdit_6.text(),
            'tax_rate': self.lineEdit_7.text()
        }
        inv_response = self.api_invoice.add_update_invoice(invoice_info)
        self.response_label.setText(inv_response['Message'])
        if 'status' in inv_response and inv_response['status'] == "success":
            self.response_label.setStyleSheet("color:green")

    def list_invoices(self,InvoiceWindow):
        InvoiceWindow.setObjectName("InvoiceWindow")
        InvoiceWindow.resize(738, 480)
        self.centralwidget = QWidget(InvoiceWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QRect(0, 30, 731, 451))
        self.tableWidget.setAutoFillBackground(False)
        self.tableWidget.setRowCount(20)
        self.tableWidget.setColumnCount(7)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setHorizontalHeaderLabels(
            ['Item', 'Number', 'Reference', 'Date', 'Activity Date', 'Due', 'Total'])
        InvoiceWindow.setCentralWidget(self.centralwidget)


class Loan_Calculator():
    def __init__(self):
        pass

    def loan_form(self,tab):
        self.frame = QFrame(tab)
        self.frame.setGeometry(QRect(80, 50, 491, 371))
        self.frame.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")
        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setGeometry(QRect(240, 70, 161, 29))
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit.setPlaceholderText("$")
        self.label = QLabel("Loan Amount",self.frame)
        self.label.setGeometry(QRect(130, 72, 91, 20))
        self.label.setObjectName("label")
        self.lineEdit_2 = QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QRect(240, 170, 161, 29))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.lineEdit_2.setPlaceholderText("years")
        self.lineEdit_3 = QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QRect(240, 120, 161, 29))
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.lineEdit_3.setPlaceholderText("%")
        self.label_2 = QLabel("Interest Rate",self.frame)
        self.label_2.setGeometry(QRect(130, 121, 91, 20))
        self.label_2.setObjectName("label_2")
        self.label_3 = QLabel("Loan Period (Years)",self.frame)
        self.label_3.setGeometry(QRect(90, 173, 131, 20))
        self.label_3.setObjectName("label_3")
        self.comboBox = QComboBox(self.frame)
        self.comboBox.setGeometry(QRect(240, 210, 161, 31))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("Monthly")
        self.comboBox.addItem("Bi-Weekly")
        self.comboBox.addItem("Weekly")
        self.label_4 = QLabel("Payment Period",self.frame)
        self.label_4.setGeometry(QRect(110, 214, 111, 20))
        self.label_4.setObjectName("label_4")
        self.pushButton = QPushButton("Calculate",self.frame)
        self.pushButton.setGeometry(QRect(210, 290, 111, 31))
        self.pushButton.setStyleSheet("background-color: rgb(0, 255, 127);\n"
                                      "color: rgb(255, 255, 255);")
        self.pushButton.setObjectName("pushButton")


class MainApp(object):
    def __init__(self):
        self.contact = Contacts()
        self.invoice = InvoiceView()
        self.loan_c = Loan_Calculator()

    def openWindow(self):
        self.window = QMainWindow()
        self.contact_form(self.window)
        self.window.show()

    def setupMainView(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowTitle("Loan Application")
        MainWindow.resize(650, 520)
        MainWindow.setMinimumSize(QSize(9, 0))
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.add_contact = QTabWidget(self.centralwidget)
        self.add_contact.setGeometry(QRect(0, 0, 650, 521))
        self.add_contact.setLayoutDirection(Qt.LeftToRight)
        self.add_contact.setAutoFillBackground(False)
        self.add_contact.setTabPosition(QTabWidget.North)
        self.add_contact.setTabShape(QTabWidget.Rounded)
        self.add_contact.setIconSize(QSize(34, 28))
        self.add_contact.setTabsClosable(False)
        self.add_contact.setMovable(False)
        self.add_contact.setObjectName("add_contact")

        self.tab = QWidget()
        self.tab.setObjectName("tab")
        self.tab.setStyleSheet("background-color: rgb(170, 255, 255);")
        self.loan_c.loan_form(tab=self.tab)
        self.add_contact.addTab(self.tab, "Loan Calculator")
        self.tab1 = QWidget()
        self.tab1.setObjectName("tab1")
        self.contact_tab_info(contact_tab=self.tab1)
        self.tab2 = QWidget()
        self.tab2.setObjectName("tab2")
        self.add_contact.addTab(self.tab2, "Companies")
        self.tab3 = QWidget()
        self.tab3.setObjectName("tab3")
        self.invoice_screen(quote_tab = self.tab3)
        self.add_contact.addTab(self.tab3, "Invoices")

        MainWindow.setCentralWidget(self.centralwidget)

    def invoice_screen(self,quote_tab):
        self.pushButton_3 = QPushButton("List Invoices",quote_tab)
        self.pushButton_3.setGeometry(QRect(280, 230, 99, 29))
        self.pushButton_3.setStyleSheet("background-color: rgb(0, 170, 255);\n"
                                        "color: rgb(255,255,255);")
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.open_list_invoice_window)
        self.comboBox = QComboBox(quote_tab)
        self.comboBox.setGeometry(QRect(210, 180, 231, 31))
        self.comboBox.setObjectName("comboBox")
        for contact in self.contact.contacts_get():
            for key, value in contact:
                if key == 'Name':
                    self.comboBox.addItem(value)
        self.pushButton_4 = QPushButton("Add Invoice",quote_tab)
        self.pushButton_4.setGeometry(QRect(70, 110, 99, 29))
        self.pushButton_4.setStyleSheet("color: rgb(255, 255, 255);\n"
                                        "background-color: rgb(170, 170, 127);")
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.open_add_invoice_window)
        self.pushButton_5 = QPushButton("Edit Invoice",quote_tab)
        self.pushButton_5.setGeometry(QRect(490, 110, 99, 29))
        self.pushButton_5.setStyleSheet("background-color: rgb(85, 170, 127);\n"
                                        "color: rgb(255, 255, 255);")
        self.pushButton_5.setObjectName("pushButton_5")
        self.label = QLabel("Select A Contact",quote_tab)
        self.label.setGeometry(QRect(210, 160, 111, 17))
        self.label.setObjectName("label")
        self.pushButton_5.raise_()
        self.pushButton_3.raise_()
        self.comboBox.raise_()
        self.pushButton_4.raise_()
        self.label.raise_()

    def open_list_invoice_window(self):
        self.window = QMainWindow()
        self.invoice.list_invoices(self.window)
        self.window.show()

    def open_add_invoice_window(self):
        self.window = QMainWindow()
        self.invoice.add_quote(self.window)
        self.window.show()

    def add_quote(self,MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(821, 512)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame_2 = QFrame(self.centralwidget)
        self.frame_2.setGeometry(QRect(420, 30, 361, 371))
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.lineEdit_3 = QLineEdit(self.frame_2)
        self.lineEdit_3.setGeometry(QRect(120, 60, 201, 29))
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.label_7 = QLabel("Item",self.frame_2)
        self.label_7.setGeometry(QRect(80, 65, 31, 17))
        self.label_7.setObjectName("label_7")
        self.plainTextEdit = QPlainTextEdit(self.frame_2)
        self.plainTextEdit.setGeometry(QRect(120, 100, 201, 75))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.label_8 = QLabel("Description",self.frame_2)
        self.label_8.setGeometry(QRect(20, 130, 81, 20))
        self.label_8.setObjectName("label_8")
        self.lineEdit_4 = QLineEdit(self.frame_2)
        self.lineEdit_4.setGeometry(QRect(120, 190, 201, 29))
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.label_9 = QLabel("Quantity",self.frame_2)
        self.label_9.setGeometry(QRect(40, 200, 61, 17))
        self.label_9.setObjectName("label_9")
        self.lineEdit_5 = QLineEdit(self.frame_2)
        self.lineEdit_5.setGeometry(QRect(120, 230, 201, 29))
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.label_10 = QLabel("Unit Price",self.frame_2)
        self.label_10.setGeometry(QRect(40, 240, 71, 20))
        self.label_10.setObjectName("label_10")
        self.lineEdit_6 = QLineEdit(self.frame_2)
        self.lineEdit_6.setGeometry(QRect(120, 270, 201, 29))
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_11 = QLabel("Account",self.frame_2)
        self.label_11.setGeometry(QRect(40, 280, 61, 20))
        self.label_11.setObjectName("label_11")
        self.lineEdit_7 = QLineEdit(self.frame_2)
        self.lineEdit_7.setGeometry(QRect(120, 310, 201, 29))
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.label_12 = QLabel("Tax Rate",self.frame_2)
        self.label_12.setGeometry(QRect(50, 320, 61, 20))
        self.label_12.setObjectName("label_12")
        self.label_13 = QLabel("Line item Information",self.frame_2)
        self.label_13.setGeometry(QRect(130, 10, 151, 31))
        self.label_13.setObjectName("label_13")
        self.frame = QFrame(self.centralwidget)
        self.frame.setGeometry(QRect(40, 30, 341, 351))
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")
        self.comboBox = QComboBox(self.frame)
        self.comboBox.setGeometry(QRect(120, 70, 181, 31))
        self.comboBox.setObjectName("comboBox")
        self.label = QLabel("Customer",self.frame)
        self.label.setGeometry(QRect(47, 75, 71, 17))
        self.label.setObjectName("label")
        self.dateEdit = QDateEdit(self.frame)
        self.dateEdit.setGeometry(QRect(120, 120, 181, 29))
        self.dateEdit.setObjectName("dateEdit")
        self.label_2 = QLabel("Date",self.frame)
        self.label_2.setGeometry(QRect(78, 125, 41, 17))
        self.label_2.setObjectName("label_2")
        self.dateEdit_2 = QDateEdit(self.frame)
        self.dateEdit_2.setGeometry(QRect(120, 170, 181, 29))
        self.dateEdit_2.setObjectName("dateEdit_2")
        self.label_3 = QLabel("Expiry",self.frame)
        self.label_3.setGeometry(QRect(68, 175, 45, 17))
        self.label_3.setObjectName("label_3")
        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setGeometry(QRect(120, 220, 181, 29))
        self.lineEdit.setObjectName("lineEdit")
        self.label_4 = QLabel("Quote Number",self.frame)
        self.label_4.setGeometry(QRect(10, 220, 101, 20))
        self.label_4.setObjectName("label_4")
        self.lineEdit_2 = QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QRect(120, 270, 181, 29))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_5 = QLabel("Reference",self.frame)
        self.label_5.setGeometry(QRect(40, 270, 71, 20))
        self.label_5.setObjectName("label_5")
        self.label_6 = QLabel("Quote Information",self.frame)
        self.label_6.setGeometry(QRect(90, 20, 131, 31))
        self.label_6.setObjectName("label_6")
        MainWindow.setCentralWidget(self.centralwidget)

    def contact_tab_info(self,contact_tab):
        self.contact_table = QTableWidget(contact_tab)
        self.contact_table.setGeometry(QRect(10, 151, 621, 281))
        self.contact_table.setMinimumSize(QSize(10, 10))
        self.contact_table.setLineWidth(10)
        self.contact_table.setRowCount(30)
        self.contact_table.setColumnCount(4)
        self.contact_table.setObjectName("contact_table")
        self.contact_table.verticalHeader().setVisible(True)
        self.contact_table.setHorizontalHeaderLabels(
            ['Name', 'EmailAddress', 'SkypeUserName', 'Phones'])
        self.names = list()
        for m in range(len(self.contact.contacts_get())):
            i = 0
            for j, v in self.contact.contacts_get()[m]:
                self.names.append(v if j == 'Name' else None)
                phone = list(filter(None,map(lambda f: f['PhoneNumber'] if 'PhoneType' in f and f['PhoneType'] == 'MOBILE' else None,v)))
                if not isinstance(v,(list,tuple)):
                    self.contact_table.setItem(m, i, QTableWidgetItem(v))
                    self.contact_table.resizeColumnsToContents()
                    self.contact_table.setEditTriggers(QTableWidget.NoEditTriggers)
                else:
                    self.contact_table.setItem(m, i, QTableWidgetItem(phone[0] if len(phone) > 0 else ''))
                i += 1

        self.names = list(filter(None,self.names))
        self.add_contact_button = QPushButton("ADD CONTACT", contact_tab)
        self.add_contact_button.setGeometry(QRect(10, 90, 111, 29))
        self.add_contact_button.setStyleSheet("background-color: rgb(85, 255, 127);\n"
                                              "color: rgb(0, 0, 0);")
        self.add_contact_button.setObjectName("add_contact_button")
        self.add_contact_button.clicked.connect(self.openWindow)
        self.add_contact.addTab(contact_tab, "Contacts")

        self.edit_contact_button = QPushButton("EDIT CONTACT", contact_tab)
        self.edit_contact_button.setGeometry(QRect(520, 90, 111, 29))
        self.edit_contact_button.setStyleSheet("background-color: rgb(85, 255, 127);\n"
                                              "color: rgb(0, 0, 0);")
        self.edit_contact_button.setObjectName("edit_contact_button")
        self.edit_contact_button.clicked.connect(self.dialog_window)
        self.add_contact.addTab(contact_tab, "Contacts")

    def contact_form(self,OtherWindow):
        OtherWindow.setObjectName("MainWindow")
        OtherWindow.resize(640, 480)
        OtherWindow.setStyleSheet("background-image: url(finance1.jpg);")
        OtherWindow.setWindowTitle("Contact Form")
        self.centralwidget = QWidget(OtherWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QFrame(self.centralwidget)
        self.frame.setGeometry(QRect(80, 50, 501, 381))
        self.frame.setStyleSheet("background-color: rgb(255, 255, 255); background-image:url(white.png)")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")
        self.responseLabel = QLabel(self.frame)
        self.responseLabel.setGeometry(10, 10, 480, 16)

        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setGeometry(QRect(210, 30, 221, 31))
        self.lineEdit.setObjectName("lineEdit")
        self.label = QLabel("First Name",self.frame)
        self.label.setGeometry(QRect(120, 35, 75, 16))
        self.label.setObjectName("First_Name")

        self.lineEdit_2 = QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QRect(210, 70, 221, 31))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_2 = QLabel("Last Name",self.frame)
        self.label_2.setGeometry(QRect(120, 75, 71, 16))
        self.label_2.setObjectName("Last_Name")

        self.lineEdit_3 = QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QRect(210, 110, 221, 31))
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.label_3 = QLabel("Mobile",self.frame)
        self.label_3.setGeometry(QRect(140, 115, 51, 16))
        self.label_3.setObjectName("Mobile")

        self.lineEdit_4 = QLineEdit(self.frame)
        self.lineEdit_4.setGeometry(QRect(210, 190, 221, 31))
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.label_4 = QLabel("Email",self.frame)
        self.label_4.setGeometry(QRect(150, 155, 41, 16))
        self.label_4.setObjectName("Email")

        self.lineEdit_5 = QLineEdit(self.frame)
        self.lineEdit_5.setGeometry(QRect(210, 150, 221, 31))
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.label_5 = QLabel("Skype Username",self.frame)
        self.label_5.setGeometry(QRect(80, 195, 115, 16))
        self.label_5.setObjectName("Skype_Username")

        self.lineEdit_6 = QPlainTextEdit(self.frame)
        self.lineEdit_6.setGeometry(QRect(210, 230, 221, 81))
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_6 = QLabel("Address",self.frame)
        self.label_6.setGeometry(QRect(130, 260, 61, 16))
        self.label_6.setObjectName("Address")

        self.pushButton = QPushButton(self.frame)
        self.pushButton.setGeometry(QRect(250, 320, 99, 29))
        self.pushButton.setStyleSheet("background-color: rgb(0, 255, 255); color: rgb(0,0,0);")
        self.pushButton.setObjectName("Add")
        self.pushButton.clicked.connect(self.post_values)
        OtherWindow.setCentralWidget(self.centralwidget)

    def dialog_window(self):
        self.window1 = QMainWindow()
        self.dialog_form_window(self.window1)
        self.window1.show()

    def dialog_form_window(self,Edit_Contact_Form):
        Edit_Contact_Form.setObjectName("Edit_Contact_Form")
        Edit_Contact_Form.resize(371, 221)
        Edit_Contact_Form.setWindowTitle("Edit Dialog")
        self.pushButton = QPushButton("Edit",Edit_Contact_Form)
        self.pushButton.setGeometry(QRect(140, 120, 99, 29))
        self.pushButton.setStyleSheet("background-color: rgb(181, 115, 255);\n"
                                      "color: rgb(255, 255, 255);")
        self.pushButton.setObjectName("Edit")
        self.pushButton.clicked.connect(self.contact_edit)
        self.comboBox = QComboBox(Edit_Contact_Form)
        self.comboBox.setGeometry(QRect(90, 80, 201, 31))
        self.comboBox.setObjectName("comboBox")
        for j in self.names:
            self.comboBox.addItem(j)
        self.label = QLabel("Select Contact To Edit :",Edit_Contact_Form)
        self.label.setGeometry(QRect(90, 50, 161, 17))
        self.label.setObjectName("label")

    def contact_edit(self):
        self.window2 = QMainWindow()
        self.Edit_contact_form(self.window2,str(self.comboBox.currentText()).strip())
        self.window2.show()

    def Edit_contact_form(self,OtherWindow,selected_value):
        OtherWindow.setObjectName("MainWindow")
        OtherWindow.resize(640, 480)
        OtherWindow.setStyleSheet("background-image: url(finance2.jpg);")
        OtherWindow.setWindowTitle("Edit Contact Form")
        self.centralwidget = QWidget(OtherWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QFrame(self.centralwidget)
        self.frame.setGeometry(QRect(80, 50, 501, 381))
        self.frame.setStyleSheet("background-color: rgb(255, 255, 255); background-image:url(white.png)")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")
        self.responseLabel = QLabel(self.frame)

        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setGeometry(QRect(210, 30, 221, 31))
        self.lineEdit.setObjectName("lineEdit")
        self.label = QLabel("First Name",self.frame)
        self.label.setGeometry(QRect(120, 35, 75, 16))
        self.label.setObjectName("First_Name")

        self.lineEdit_2 = QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QRect(210, 70, 221, 31))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_2 = QLabel("Last Name",self.frame)
        self.label_2.setGeometry(QRect(120, 75, 71, 16))
        self.label_2.setObjectName("Last_Name")

        self.lineEdit_3 = QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QRect(210, 110, 221, 31))
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.label_3 = QLabel("Mobile",self.frame)
        self.label_3.setGeometry(QRect(140, 115, 51, 16))
        self.label_3.setObjectName("Mobile")

        self.lineEdit_4 = QLineEdit(self.frame)
        self.lineEdit_4.setGeometry(QRect(210, 190, 221, 31))
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.label_4 = QLabel("Email",self.frame)
        self.label_4.setGeometry(QRect(150, 155, 41, 16))
        self.label_4.setObjectName("Email")

        self.lineEdit_5 = QLineEdit(self.frame)
        self.lineEdit_5.setGeometry(QRect(210, 150, 221, 31))
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.label_5 = QLabel("Skype Username",self.frame)
        self.label_5.setGeometry(QRect(80, 195, 115, 16))
        self.label_5.setObjectName("Skype_Username")

        self.lineEdit_6 = QPlainTextEdit(self.frame)
        self.lineEdit_6.setGeometry(QRect(210, 230, 221, 81))
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_6 = QLabel("Address",self.frame)
        self.label_6.setGeometry(QRect(130, 260, 61, 16))
        self.label_6.setObjectName("Address")

        edit_info = dict(self.contact.contacts_get(condition=selected_value))
        self.lineEdit.setText(edit_info['FirstName'])
        self.lineEdit_2.setText(edit_info['LastName'])
        self.lineEdit_3.setText( [j['PhoneNumber'] for j in edit_info['Phones'] if j['PhoneType'] == 'MOBILE'][0] )
        self.lineEdit_4.setText(edit_info['EmailAddress'])
        self.lineEdit_5.setText(edit_info['SkypeUserName'])
        # self.lineEdit_6.setText(edit_info['Address'])

        self.pushButton = QPushButton(self.frame)
        self.pushButton.setGeometry(QRect(250, 320, 99, 29))
        self.pushButton.setStyleSheet("background-color: rgb(0, 255, 255); color: rgb(0,0,0);")
        self.pushButton.setObjectName("Add")
        self.pushButton.clicked.connect(self.update_values)
        OtherWindow.setCentralWidget(self.centralwidget)

    def update_values(self):
        conatct_info = {
            'FirstName': self.lineEdit.text(),
            'LastName': self.lineEdit_2.text(),
            'Phone': self.lineEdit_3.text(),
            'SkypeUserName': self.lineEdit_5.text(),
            'EmailAddress': self.lineEdit_4.text(),
            'address': self.lineEdit_6.toPlainText(),
        }
        self.contact.contact_add_update(conatct_info,type="update")

    def post_values(self):
        conatct_info = {
            'FirstName': self.lineEdit.text(),
            'LastName': self.lineEdit_2.text(),
            'Phone': self.lineEdit_3.text(),
            'SkypeUserName': self.lineEdit_4.text(),
            'EmailAddress': self.lineEdit_5.text(),
            'address': self.lineEdit_6.toPlainText(),
        }
        contact_response = self.contact.contact_add_update(conatct_info)
        self.responseLabel.setText(contact_response['Message'])
        if 'ErrorNo' in contact_response:
            self.responseLabel.setStyleSheet("color:red")
        else:
            self.responseLabel.setStyleSheet("color:green")


if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    ui = MainApp()
    ui.setupMainView(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
