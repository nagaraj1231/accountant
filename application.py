import sys, random
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QLineEdit, QLabel, QComboBox, QPushButton, QTabWidget, QVBoxLayout, QTableWidget, \
    QTableWidgetItem, QHBoxLayout, QSizePolicy, QStatusBar, QFrame, QDialog
from PyQt5.QtGui import *
from PyQt5.QtCore import pyqtSlot, QRect, QCoreApplication
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from api import Contacts


class Graph_Window(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        FigureCanvas.__init__(self, fig)
        self.setParent(None)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.plot_graph()

    def plot_graph(self):
        data = [random.random() for i in range(25)]
        ax = self.figure.add_subplot(111)
        ax.plot(data, 'r-')
        ax.set_title('Loan Graph')
        self.draw()

class AccountantApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Loan Calculator")
        pass

    def form_widget(self):
        self.label1 = QLabel(self)
        self.label1.move(210,40)
        self.label1.setText("Loan Amount :")
        self.text1 = QLineEdit(self)
        self.text1.move(320,40)
        self.text1.resize(300,35)

        self.label2 = QLabel(self)
        self.label2.move(210,100)
        self.label2.setText("Interest Rate :")
        self.text2 = QLineEdit(self)
        self.text2.move(320,100)
        self.text2.resize(300,35)

        self.label3 = QLabel(self)
        self.label3.move(170,150)
        self.label3.resize(160,30)
        self.label3.setText("Loan Period (Years) :")
        self.text3 = QLineEdit(self)
        self.text3.move(320,150)
        self.text3.resize(300,35)

        self.label4 = QLabel(self)
        self.label4.move(190,200)
        self.label4.setText("Payment Period :")
        self.label4.resize(160, 30)
        self.cb = QComboBox(self)
        self.cb.addItem("Monthly")
        self.cb.addItem("Bi-Weekly")
        self.cb.addItem("Weekly")
        self.cb.move(320,200)
        self.cb.resize(300,35)

        self.btn1 = QPushButton("Calculate", self)
        self.btn1.setToolTip("Calculate the total amount")
        self.btn1.setStyleSheet("background-color : teal; color: white")
        self.btn1.move(320,250)
        self.dialog = list()
        self.btn1.clicked.connect(self.new_window)
        return self

    def new_window(self):
        gp = Graph_Window()
        self.dialog.append(gp)
        gp.show()

    def Table_Widget(self):
        self.table_widget = QTableWidget(4,4)
        self.table_widget.verticalHeader().setVisible(False)
        self.table_widget.move(12,12)
        self.table_widget.resize(12, 12)
        self.table_widget.setHorizontalHeaderLabels(['Date','Principal','Interest','Balance'])
        table_inputs = [{'Date':'Aug, 2019','Principal': '190.33','Interest':'20.00','Balance':'11,809.67'},
                        {'Date':'Sep, 2019','Principal': '190.65','Interest':'19.68','Balance':'11,619.02'},
                        {'Date':'Oct, 2019','Principal': '190.97','Interest':'19.37','Balance':'11,428.05'}]

        for i,j in enumerate(table_inputs):
            self.table_widget.setItem(i,0,QTableWidgetItem(j["Date"]))
            self.table_widget.setItem(i,1, QTableWidgetItem(j["Principal"]))
            self.table_widget.setItem(i,2, QTableWidgetItem(j["Interest"]))
            self.table_widget.setItem(i,3, QTableWidgetItem(j["Balance"]))

        return self.table_widget

class ContactsView():
    def __init__(self):
        super().__init__()

    def get_contacts_view(self):
        MainWindow = QMainWindow()
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 480)
        # contact = Contacts()
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QRect(3, 90, 750, 361))
        self.tableWidget.setRowCount(30)
        self.tableWidget.setColumnCount(6)
        self.tableWidget.setHorizontalHeaderLabels(['Name', 'FirstName', 'LastName', 'EmailAddress', 'SkypeUserName','Action'])
        self.tableWidget.setObjectName("tableWidget")
        # for m in range(len(contact.contacts_get())):
        #     i = 0
        #     for j,v in contact.contacts_get()[m]:
        #         self.tableWidget.setItem(m, i, QTableWidgetItem(v))
        #         self.tableWidget.resizeColumnsToContents()
        #         i += 1
        MainWindow.setCentralWidget(self.centralwidget)
        self.btn1 = QPushButton("Add Contact", self.centralwidget)
        self.btn1.setToolTip("Clicking on this button will open a form to add contact")
        self.btn1.setStyleSheet("background-color : #50cd52; color: Black;")
        self.btn1.move(3,25)
        self.btn1.clicked.connect(self.add_form_view)
        return MainWindow

    def add_form_view(self):
        self.window = QMainWindow()
        self.ui = self.contact_form_add()
        self.ui.setupUi(self.window)
        self.window.show()

    def contact_form_add(self):
        MainWindow = QMainWindow()
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 480)
        MainWindow.setStyleSheet("background-color: rgb(243, 212, 255);")
        MainWindow.setTabShape(QTabWidget.Rounded)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QFrame(self.centralwidget)
        self.frame.setGeometry(QRect(70, 30, 511, 421))
        self.frame.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setObjectName("frame")
        self.lineEdit_2 = QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QRect(200, 20, 231, 41))
        self.lineEdit_2.setStyleSheet("border-color: rgb(0, 0, 0);\n"
                                      "background-color: rgb(255, 255, 255);")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label = QLabel(self.frame)
        self.label.setGeometry(QRect(110, 30, 81, 20))
        self.label.setObjectName("label")
        self.lineEdit_3 = QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QRect(200, 80, 231, 41))
        self.lineEdit_3.setStyleSheet("border-color: rgb(0, 0, 0);\n"
                                      "background-color: rgb(255, 255, 255);")
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.label_2 = QLabel(self.frame)
        self.label_2.setGeometry(QRect(110, 90, 81, 20))
        self.label_2.setObjectName("label_2")
        self.lineEdit_4 = QLineEdit(self.frame)
        self.lineEdit_4.setGeometry(QRect(200, 140, 231, 41))
        self.lineEdit_4.setStyleSheet("border-color: rgb(0, 0, 0);\n"
                                      "background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.label_3 = QLabel(self.frame)
        self.label_3.setGeometry(QRect(90, 150, 101, 20))
        self.label_3.setObjectName("label_3")
        self.lineEdit = QLineEdit(self.frame)
        self.lineEdit.setGeometry(QRect(200, 200, 231, 41))
        self.lineEdit.setStyleSheet("border-color: rgb(0, 0, 0);\n"
                                    "background-color: rgb(255, 255, 255);")
        self.lineEdit.setObjectName("lineEdit")
        self.label_4 = QLabel(self.frame)
        self.label_4.setGeometry(QRect(90, 210, 101, 20))
        self.label_4.setObjectName("label_4")
        self.lineEdit_5 = QLineEdit(self.frame)
        self.lineEdit_5.setGeometry(QRect(200, 260, 231, 91))
        self.lineEdit_5.setStyleSheet("border-color: rgb(0, 0, 0);\n"
                                      "background-color: rgb(255, 255, 255);")
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.label_5 = QLabel(self.frame)
        self.label_5.setGeometry(QRect(130, 290, 61, 20))
        self.label_5.setObjectName("label_5")
        self.pushButton = QPushButton(self.frame)
        self.pushButton.setGeometry(QRect(260, 360, 99, 29))
        self.pushButton.setStyleSheet("background-color: rgb(255, 85, 255);\n"
                                      "color: rgb(255, 255, 255);")
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)
        return MainWindow


class MainApp(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Loan Calculator")
        self.setGeometry(10, 10, 800, 400)
        self.position_window_center()
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tab4 = QWidget()

    def add_tabs(self):
        calculator = AccountantApp()
        contacts = ContactsView()
        self.layout = QVBoxLayout(self)
        self.tabs.resize(300,200)
        self.tabs.addTab(self.tab1,"Loan Calculate Form")
        self.tabs.addTab(self.tab2,"Table")
        self.tabs.addTab(self.tab3, "Graph")
        self.tabs.addTab(self.tab4, "Contacts")

        self.tab1.layout = QVBoxLayout(self)
        self.tab1.layout.addWidget(calculator.form_widget())
        self.tab1.setLayout(self.tab1.layout)

        self.tab2.layout = QHBoxLayout(self)
        self.tab2.layout.addWidget(calculator.Table_Widget())
        self.tab2.setLayout(self.tab2.layout)

        self.tab3.layout = QHBoxLayout(self)
        self.tab3.layout.addWidget(Graph_Window(width=5,height=4))
        self.tab3.setLayout(self.tab3.layout)

        self.tab4.layout = QHBoxLayout(self)
        self.tab4.layout.addWidget(contacts.get_contacts_view())
        self.tab4.setLayout(self.tab4.layout)

        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

        self.show()

    def position_window_center(self):
        frame = self.frameGeometry()
        screen = QApplication.desktop().screenNumber(QApplication.desktop().cursor().pos())
        center_point = QApplication.desktop().screenGeometry(screen).center()
        frame.moveCenter(center_point)
        self.move(frame.topLeft())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    acc = MainApp()
    acc.add_tabs()
    app.exec_()