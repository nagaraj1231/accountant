from settings import *
from requests_oauthlib import OAuth1Session, OAuth1
import json,requests
from models import contacts, engine, invoices
from sqlalchemy.sql import select

class Xero_App():
    def __init__(self):
        self.request_token_url = REQUEST_TOKEN_URL
        self.authorization_url = AUTHORIZATION_URL
        self.acess_token_url = ACCESS_TOKEN_URL
        self.consumer_key = CONSUMER_KEY
        self.consumer_secret = CONSUMER_SECRET

    def generate_token(self):
        xero_app = OAuth1Session(client_key=self.consumer_key,
                                 client_secret=self.consumer_secret,
                                 signature_method = "HMAC-SHA1")
        response = xero_app.fetch_request_token(self.request_token_url)
        print(response)
        authorized = xero_app.authorization_url(self.authorization_url)
        print(authorized)
        return authorized

    def get_access_token(self):
        xero_access = OAuth1Session(client_key=self.consumer_key,
                                    client_secret=self.consumer_secret,
                                    resource_owner_key=ACESS_TOKEN,
                                    resource_owner_secret=ACESS_TOKEN_SECRET,
                                    verifier=CODE,
                                    signature_method = "HMAC-SHA1")
        access_token = xero_access.fetch_access_token(self.acess_token_url)
        return access_token['oauth_token'], access_token['oauth_token_secret']

    def process_api_request(self,api,type,data=""):
        url = "https://api.xero.com/api.xro/2.0/{}".format(api)
        request = OAuth1Session(client_key=self.consumer_key,
                                client_secret=self.consumer_secret,
                                resource_owner_key= LAST_ACESS_TOKEN,
                                resource_owner_secret= LAST_ACESS_SECRET,
                                signature_method = "HMAC-SHA1"
                                )
        auth = OAuth1(
            client_key=self.consumer_key,
            client_secret=self.consumer_secret,
            resource_owner_key=LAST_ACESS_TOKEN,
            resource_owner_secret=LAST_ACESS_SECRET,
            signature_method="HMAC-SHA1"
        )
        if type == "PUT":
            response = requests.put(url,auth=auth,data=json.dumps(dict(data)),headers = {'Accept':'application/json'})
            # response = request.put(url, data=json.dumps(dict(data)), headers = {'Accept':'application/json'})
        else:
            response = request.get(url, headers = {'Accept':'application/json'})
        return response.text

class Contacts(Xero_App):
    def __init__(self):
        super().__init__()
        self.conn = engine.connect()

    def contacts_get(self, condition=''):
        contacts_info = json.loads(self.process_api_request(api="Contacts",type="GET"))
        resp = list()
        required_fields = ['Name','EmailAddress','SkypeUserName',"Phones"]
        if not condition:
            response = [ (c,v) for contact_info in contacts_info["Contacts"] for c,v in contact_info.items() if c in required_fields ]
            resp.append(response)
        else:
            required_fields.extend(['FirstName','LastName'])
            response = [(c, v) for contact_info in contacts_info["Contacts"] for c, v in contact_info.items() if
                        c in required_fields and contact_info['Name'] == str(condition).strip()]
            resp.extend(response)

        return resp

    def contact_id_get(self,condition):
        contacts_info = json.loads(self.process_api_request(api="Contacts", type="GET"))
        contact_response = [ contact_info['ContactID'] for contact_info in contacts_info["Contacts"] if contact_info['Name'] == str(condition).strip() ]
        return contact_response[0]

    def contact_add_update(self,contact_info,type=""):
        if 'address' in contact_info and contact_info['address']:
            contact_info.update({"Addresses":[
                {
                'AddressType':'POBOX',
                'AddressLine1': contact_info['address']
                }]
            })
        if 'Phone' in contact_info and contact_info['Phone']:
            contact_info.update({'Phones':[
                {
                    'PhoneType' : "MOBILE",
                    'PhoneNumber': contact_info['Phone']
                }
            ]})
        if contact_info['FirstName'] and contact_info['LastName']:
            contact_info.update({'Name': contact_info['FirstName']+" "+contact_info['LastName']})
        contact_info.pop('Phone')
        contact_info.pop('address')
        if not type:
            response = self.process_api_request(api="Contacts",type="PUT",data=contact_info)
            response = json.loads(response)
            if 'ErrorNumber' in response:
                final_output = {'ErrorNo': response['ErrorNumber'],
                                "Message": response['Elements'][0]['ValidationErrors'][0]['Message']}
            else:
                final_output = {'status': "success", "Message": "Inserted Successfully", 'id': response['Contacts'][0]['ContactID']}
                self.conn.execute(contacts.insert(),[{'conatctID':final_output['id'],
                                                      'email':contact_info['EmailAddress'],
                                                      'username':contact_info['Name']}])

            return final_output
        else:
            result = select([contacts]).where(contacts.c.email == contact_info['EmailAddress'])
            result = list(self.conn.execute(result))
            contact_response = self.process_api_request(api="Contacts/{}".format(result[0][1]), type="PUT", data=contact_info)
            print(contact_response)


class Invoices(Xero_App):
    def __init__(self):
        super().__init__()
        self.contact = Contacts()
        self.conn = engine.connect()

    def add_update_invoice(self,invoice_info):
        contact_response = select([contacts]).where(contacts.c.username == invoice_info['customer'])
        contact_response = list(self.conn.execute(contact_response))
        if contact_response:
            request = {
                "Type" : "ACCREC",
                "Contact" : {
                    "ContactID" : str(contact_response[0][1])
                },
                "LineAmountTypes" : "Exclusive",
                "LineItems" : [
                    {
                        "Description": invoice_info['description'],
                        "Quantity": invoice_info['quantity'],
                        "UnitAmount": invoice_info['price'],
                        "AccountCode": invoice_info['account'],
                        "TaxAmount": invoice_info['tax_rate']
                    }
                ]
            }
            invoice_response = self.process_api_request(api="Invoices", type="PUT", data=request)
            invoice_response = json.loads(invoice_response)
            self.conn.execute(invoices.insert(),[{
                'InvoiceID': invoice_response["Invoices"][0]["InvoiceID"],
                'contact_id' : contact_response[0][0]
            }])
            if 'Status' in invoice_response:
                final_output = {'status': "success", "Message": "Invoice created successfully for {}".format(invoice_info['customer']),
                                'id': invoice_response["Invoices"][0]["InvoiceID"]}
            else:
                final_output = {'status': "error", "Message": "There is some problem with the response",
                                'id': invoice_response["Invoices"][0]["InvoiceID"]}

            print(invoice_response)
            return final_output

    def invoices_get(self):
        invoice_response = self.process_api_request(api="Invoices",type="GET")
        print(invoice_response)
        exit()




