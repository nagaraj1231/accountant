from sqlalchemy import create_engine
from sqlalchemy import Table, Column, String, Integer, MetaData
from settings import *

meta = MetaData()
# engine = create_engine("mysql://root:root@localhost/accountant")
engine = create_engine("{engine}://{user}:{password}@{host}/{db}".format(user=DB_USER,
                                                                         password=DB_PASSWORD,
                                                                         engine=DB_ENGINE,
                                                                         host=DB_HOST,
                                                                         db=DB_NAME))
contacts = Table(
    'contacts', meta,
    Column('id',Integer,primary_key=True),
    Column('conatctID',String(100)),
    Column('email',String(50)),
    Column('username',String(50))
)

invoices = Table(
    'invoices', meta,
    Column('id',Integer,primary_key=True),
    Column('InvoiceID',String(100)),
    Column('contact_id',Integer)
)

meta.create_all(engine)

